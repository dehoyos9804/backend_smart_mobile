import googlemaps
from datetime import  datetime
from .utilitarios import Utilitarios
from backend_smartmobile.settings import DATABASE_ENV

#leer el key de google maps
class GoogleMapsConfig(object):
    gmaps = None
    API_KEY = None

    def __init__(self):
        config = Utilitarios.read_file_json('./config.json')
        if DATABASE_ENV.__eq__("LOCAL"):
            self.API_KEY = config['LOCAL']['API_KEY_GOOGLE']
        elif DATABASE_ENV.__eq__("CLOUD"):
            self.API_KEY = config['CLOUD']['API_KEY_GOOGLE']

        self.gmaps = googlemaps.Client(key=self.API_KEY)

    def get_gmc(self):
        return self.gmaps
