import json
from subprocess import Popen, PIPE, STDOUT
from backend_smartmobile.settings import DATABASES
import zipfile
from os import scandir, getcwd
from os.path import abspath, basename, splitext

class Utilitarios(object):

    @classmethod
    def read_file_json(self, path):
        with open('./config.json', 'r') as file:
            config = json.load(file)
            return config

class Zip(object):
    @classmethod
    def check_shp(self, path: str, type_extension: str):
        '''
        permite checkear si un archivo zip contiene un archivo con la extension solicitada
        :param path: ruta del archivo zip
        :param type_extension: typo de extension que se necesita verificar
        '''

        try:
            shp_file_exists = False

            with zipfile.ZipFile(path, 'r') as archive_zip:
                #almacenamos la lista de archivos en una variable
                list_file = archive_zip.namelist()

                #recorrer la lista y verificar si existe algún archivo con extension .shp
                for x in range(0, len(list_file)):
                    extension = list_file[x].split('.')[-1]
                    if extension.__eq__(type_extension):
                        shp_file_exists = True

            archive_zip.close()

            return shp_file_exists

        except Exception as e:
            print('ERROR', str(e))
            return str(e)

    @classmethod
    def unzip_file(self, path: str, name: str):
        '''
        permite extraer un archivo zip en una carpeta especificada
        :param path: ruta del archivo zip
        :param name: ruta de la carpeta donde se guardarán los archivos descomprimidos
        :return: True, False
        '''

        try:
            unzip_success = False

            with zipfile.ZipFile(path, 'r') as archive_zip:
                archive_zip.extractall(pwd=None, path=name)
                unzip_success = True

            archive_zip.close()

            return unzip_success

        except Exception as e:
            print('ERROR', str(e))
            return str(e)


class Shp2pgsql(object):
    @classmethod
    def run_subproceso(self, srid: str, path_absolute_shp: str, name_schema: str, name_table: str):
        '''Importe shapefile en Postgresql usando 'shp2pgsql' (línea de comando pasada a través de 'Popen')'''
        try:
            database = DATABASES['default']['NAME']
            user_database = DATABASES['default']['USER']
            password_database = DATABASES['default']['PASSWORD']

            p1 = Popen(['shp2pgsql', '-s', '%s:4326'%srid, '-d', '-I', path_absolute_shp, name_schema + "." + name_table],
                       stdout=PIPE, stderr=PIPE)

            p2 = Popen(["psql", "-U", user_database, "-d", database], stdin=p1.stdout, stdout=PIPE, stderr=PIPE)
            p1.stdout.close()

            stdout, stderr = p2.communicate()

            #print("############ STANDART OUTPUT ############" + "\n\n\n", stdout)
            if stderr:
                #print("############ STANDART ERROR ############" + "\n\n\n", stderr)
                return [True, stderr]
            else:
                #print("############ NO ERROR TO PRINT ############")
                return [True, stderr]

        except Exception as e:
            #print('ERROR', str(e))
            return str(e)

class PathFile(object):
    @classmethod
    def get_file_extension(self, ruta_folder: str, extension_get: str):
        '''
        devuelve el la ruta del archivo y nombre que tenga la extension solicitada en una carpeta de archivos
        :param ruta_folder: folder donde reposan los diferentes archivos
        :param extension_get: tipo de archivos que tengan la extension solicitada
        :return: [path, name]
        '''
        try:
            file_folder_type_file = [abspath(arch.path) for arch in scandir(ruta_folder) if arch.is_file()]
            get_file = []

            #recorro la lista de todos los archivos que se encuentra en la carpeta
            for x in range(0, len(file_folder_type_file)):
                path_final = file_folder_type_file[x]
                extension = file_folder_type_file[x].split('.')[-1]
                if extension.__eq__(extension_get):
                    #agregar el path del archivo
                    get_file.append(path_final)
                    #agregar el nombre del archivo
                    get_file.append(splitext(basename(file_folder_type_file[x]))[0])

            return get_file

        except Exception as e:
            print('ERROR', str(e))
            return str(e)