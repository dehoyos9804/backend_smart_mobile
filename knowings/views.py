from __future__ import print_function, unicode_literals
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework import status
import json
from .models import Knowing, LogFile
from .serializers import (CreateKnowingSerializer, CreateLogRequestSerializer,
                          GeocoderSerializer, PluckingSerializer, TableColumnSerializer,
                          LogFileSerializer, GeoJsonSerializer)
from utils.google_maps import GoogleMapsConfig
from backend_smartmobile.settings import FILTER_TABLE
from utils.utilitarios import Shp2pgsql, Zip, PathFile
from django.db import connection
from django.forms.models import model_to_dict
from datetime import datetime


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def geocodificar_base(request):
    try:
        '''
            permite geodicaficar una dirección 
        '''
        if request.method == 'POST':
            serializers = GeocoderSerializer(data=request.data)

            if serializers.is_valid():
                address = ""
                city = ""
                zoning_base = ""
                zoning_value = ""

                if 'address' in request.data:
                    address = request.data['address']

                if 'city' in request.data:
                    city = request.data['city']

                if 'zoning_base' in request.data:
                    zoning_base = request.data['zoning_base'].strip()

                if 'zoning_value' in request.data:
                    zoning_value = request.data['zoning_value'].strip()

                formatted_address_query = address + ", " + city

                query = '''SELECT *
                                    from knowings_knowing 
                                    WHERE upper(address || ', ' || city) LIKE %s '''

                knowing = Knowing.objects.raw(query, [formatted_address_query.upper()])

                if knowing:
                    # guardamos el log de consulta
                    issavelog = save_log_request(knowing[0].address, knowing[0].city, knowing[0].cx, knowing[0].cy,
                                                 knowing[0].formatted_address,
                                                 knowing[0].query_type, zoning_base, zoning_value, request.user.id)
                    data_final = {
                        'address': knowing[0].address,
                        'city': knowing[0].city,
                        'cx': knowing[0].cx,
                        'cy': knowing[0].cy,
                        'formatted_address': knowing[0].formatted_address,
                    }
                    if issavelog == True:
                        if zoning_base != '' and zoning_value != '':
                            zonifi = zonification(zoning_base, zoning_value, knowing[0].cx, knowing[0].cy)

                            data_final['data_zonificado'] = zonifi
                        else:
                            data_final['data_zonificado'] = False

                        return Response({'data': data_final}, status=status.HTTP_200_OK)
                else:
                    # consulto en geocoder
                    gmc = GoogleMapsConfig()
                    result = gmc.get_gmc().geocode(formatted_address_query, None, None, '.co', 'es')

                    knowing_data = {
                        'address': address,
                        'city': city,
                        'cx': str(result[0]['geometry']['location']['lat']),
                        'cy': str(result[0]['geometry']['location']['lng']),
                        'formatted_address': str(result[0]['formatted_address']),
                        'query_type': 'ge'
                    }
                    seri = CreateKnowingSerializer(data=knowing_data)

                    if seri.is_valid():
                        data_final = {
                            'address': address,
                            'city': city,
                            'cx': str(result[0]['geometry']['location']['lat']),
                            'cy': str(result[0]['geometry']['location']['lng']),
                            'formatted_address': (result[0]['formatted_address']),
                        }
                        # guardamos el log de datos
                        is_save_log = save_log_request(address, city, str(result[0]['geometry']['location']['lat']),
                                                       str(result[0]['geometry']['location']['lng']),
                                                       str(result[0]['formatted_address']),
                                                       'ge', zoning_base, zoning_value, request.user.id)
                        if is_save_log == True:
                            # si los datos logran pasar la validación procedemos a guardarlo
                            seri.save()
                            if zoning_base != '' and zoning_value != '':
                                zonification_data = zonification(zoning_base, zoning_value,
                                                             str(result[0]['geometry']['location']['lat']),
                                                             str(result[0]['geometry']['location']['lng']))

                                data_final['data_zonificado'] = zonification_data
                            else:
                                data_final['data_zonificado'] = False
                            return Response({'data': data_final}, status=status.HTTP_200_OK)
                        else:
                            return Response({
                                'title': 'Not Acceptable',
                                'code': '406',
                                'message': is_save_log.errors
                            }, status=status.HTTP_406_NOT_ACCEPTABLE)

                    else:
                        return Response(seri.errors, status=status.HTTP_406_NOT_ACCEPTABLE)

            else:
                return Response(serializers.errors, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def plucking(request):
    try:
        '''
        permite geocodificar una latitud y longitud 
        '''
        if request.method == 'POST':
            serializers = PluckingSerializer(data=request.data)

            if serializers.is_valid():
                cx = serializers.data.get('cx')
                cy = serializers.data.get('cy')
                zoning_base = ""
                zoning_value = ""
                if 'zoning_base' in request.data:
                    zoning_base = request.data['zoning_base'].strip()

                if 'zoning_value' in request.data:
                    zoning_value = request.data['zoning_value'].strip()

                query = '''
                            SELECT *
                            FROM knowings_knowing 
                            WHERE cx=%s
                            AND cy=%s
                        '''
                knowing = Knowing.objects.raw(query, [str(cx), str(cy)])

                if knowing:
                    # guardamos el log de consulta
                    issavelog = save_log_request(knowing[0].address, knowing[0].city, knowing[0].cx, knowing[0].cy,
                                                 knowing[0].formatted_address,
                                                 knowing[0].query_type, zoning_base, zoning_value, request.user.id)

                    data_final = {
                        'address': knowing[0].address,
                        'city': knowing[0].city,
                        'cx': knowing[0].cx,
                        'cy': knowing[0].cy,
                        'formatted_address': knowing[0].formatted_address,
                    }

                    if issavelog == True:
                        if zoning_base != '' and zoning_value != '':
                            zonifi = zonification(zoning_base, zoning_value, knowing[0].cx, knowing[0].cy)

                            data_final['data_zonificado'] = zonifi
                        else:
                            data_final['data_zonificado'] = False

                        return Response({'data': data_final}, status=status.HTTP_200_OK)
                else:
                    # consulto en geocoder
                    gmc = GoogleMapsConfig()
                    result = gmc.get_gmc().reverse_geocode((cy, cx), None, None, 'es')

                    city = ""
                    if len(result[0]['address_components']) > 1:
                        city = str(result[0]['address_components'][1]['long_name'])
                    else:
                        city = str(result[0]['address_components'][0]['long_name'])

                    knowing_data = {
                        'address': str(result[0]['formatted_address']),
                        'city': city,
                        'cx': str(result[0]['geometry']['location']['lat']),
                        'cy': str(result[0]['geometry']['location']['lng']),
                        'formatted_address': str(result[0]['formatted_address']),
                        'query_type': 'pm'
                    }
                    seri = CreateKnowingSerializer(data=knowing_data)

                    if seri.is_valid():
                        data_final = {
                            'address': str(result[0]['formatted_address']),
                            'city': city,
                            'cx': str(result[0]['geometry']['location']['lat']),
                            'cy': str(result[0]['geometry']['location']['lng']),
                            'formatted_address': (result[0]['formatted_address']),
                        }

                        # guardamos el log de datos
                        is_save_log = save_log_request(str(result[0]['formatted_address']), city, str(result[0]['geometry']['location']['lat']),
                                                       str(result[0]['geometry']['location']['lng']),
                                                       str(result[0]['formatted_address']),
                                                       'pm', zoning_base, zoning_value, request.user.id)
                        if is_save_log == True:
                            # si los datos logran pasar la validación procedemos a guardarlo
                            seri.save()
                            if zoning_base != '' and zoning_value != '':
                                zonification_data = zonification(zoning_base, zoning_value,
                                                             str(result[0]['geometry']['location']['lng']),
                                                             str(result[0]['geometry']['location']['lat']))

                                data_final['data_zonificado'] = zonification_data
                            else:
                                data_final['data_zonificado'] = False
                            return Response({'data': data_final}, status=status.HTTP_200_OK)
                        else:
                            return Response({
                                'title': 'Not Acceptable',
                                'code': '406',
                                'message': is_save_log.errors
                            }, status=status.HTTP_406_NOT_ACCEPTABLE)

                    else:
                        return Response(seri.errors, status=status.HTTP_406_NOT_ACCEPTABLE)
            else:
                return Response(serializers.errors, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


def save_log_request(address, city, cx, cy, formatted_address, query_type, zoning_base, zoning_value, user_id):
    data = {
        'address': address,
        'city': city,
        'cx': cx,
        'cy': cy,
        'formatted_address': formatted_address,
        'query_type': query_type,
        'zoning_base': zoning_base,
        'zoning_value': zoning_value,
        'user': user_id
    }
    serializer = CreateLogRequestSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return True
    else:
        print(serializer.errors)
        return serializer


def zonification(zoning_base, zoning_value, cx, cy):
    campos = zoning_value.split(',')

    query = 'SELECT 1 as id,' + str(zoning_value) + ' FROM ' + str(zoning_base) + ' WHERE st_intersects(geom, ST_GeomFromText(\'POINT (' + cy + ' ' + cx + ')\', 4326))'

    #print(query)
    zonificacion = Knowing.objects.raw(query)

    if zonificacion:
       datas = {}
       for c in campos:
            datas[c.strip()] = getattr(zonificacion[0], c.strip())

       return datas

    else:
        return False

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def table_list(request):
    try:
        '''
        devuelve la lista de todas las tablas que se pueden zonificar
        '''
        if request.method == 'GET':

            filter = ''

            for x in range(0, len(FILTER_TABLE)):
                filter += '\'' + FILTER_TABLE[x] + '\','

            if len(filter) > 0:
                filter = filter.rstrip(filter[-1])

            query = '''
                    SELECT 1 as id, table_name
                    from information_schema.tables
                    WHERE table_schema='public'
                    AND table_type='BASE TABLE'
                    and TABLE_NAME not IN(
                    ''' + filter + '''
                    )
                    ORDER BY table_name
                    '''

            result = Knowing.objects.raw(query)

            data = []

            for r in result:
                data.append(r.table_name)

            return Response({'tables': data}, 200)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo GET'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def table_list_column(request, table_name):
    try:
        '''
        devuelve la lista de todas las columnas de una tabla para zonificar el valor
        '''
        if request.method == 'GET':
            data_final = {
                'table_name': table_name
            }
            serializers = TableColumnSerializer(data=data_final)

            if serializers.is_valid():

                query = '''
                        SELECT 1 as id, column_name
                        FROM information_schema.columns
                        WHERE table_schema = \'public\'
                        AND table_name  =  %s
                        '''

                result = Knowing.objects.raw(query, [serializers.data.get('table_name')])

                data = []
                for r in result:
                    data.append(r.column_name)

                return Response({
                    'table': serializers.data.get('table_name'),
                    'columns': data
                }, 200)

            else:
                return Response(serializers.errors, status=status.HTTP_406_NOT_ACCEPTABLE)

        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo GET'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
@parser_classes([MultiPartParser, FormParser])
def uploader_file_shp(request):
    try:
        '''
        subir un archivo de tipo shp y luego subirlo a la base de datos de postgresql
        '''
        if request.method == 'POST':
            data = request.data
            data['content_type'] = request.FILES['file'].content_type
            data['size'] = request.FILES['file'].size
            data['extension'] = request.FILES['file'].name.split('.')[-1]
            data['user'] = request.user.id
            data['name'] = request.FILES['file'].name.split('.')[0]
            extension = request.FILES['file'].name.split('.')[-1]
            #print(request.FILES['file'].name.split('.')[0])

            file_serializer = LogFileSerializer(data=data)
            if file_serializer.is_valid():
                if extension.__eq__("zip"):
                    #guardamos el archivo en bases de datos para posteriores auditorias
                    instance = file_serializer.save()
                    #verificamos que el archivo zip contenga los archivos necesarios de un shp
                    existe_shp = Zip.check_shp(instance.file.path, "shp")
                    if existe_shp == True:

                        #validaciones dentro del zip
                        if Zip.check_shp(instance.file.path, "dbf") != True:
                            return Response({
                                'title': 'Error',
                                'message': 'Falta complementto dbf (datos de atributos) en el zip',
                                'existe shp': existe_shp
                            }, status=status.HTTP_406_NOT_ACCEPTABLE)

                        if Zip.check_shp(instance.file.path, "shx") != True:
                            return Response({
                                'title': 'Error',
                                'message': 'Falta complementto shx (archivo de índice de geometría) en el zip',
                                'existe shp': existe_shp
                            }, status=status.HTTP_406_NOT_ACCEPTABLE)

                        #ruta de la carpeta donde se descomprimirá
                        path_unzip_file = instance.file.path.replace(str(instance.file), "")
                        path_unzip_file = path_unzip_file + instance.name + "\\" #c:\\user\\documento\\tabla
                        #descomprimimos el archivo zip
                        unzip = Zip.unzip_file(instance.file.path, path_unzip_file)

                        if unzip == True:
                            check_file_shp = PathFile.get_file_extension(path_unzip_file, "shp")

                            if len(check_file_shp) > 0:
                                #@TODO: importante recordar que check_file_shp[0] devuelve el path del archivo, check_file_shp[1] devuelve el nombre
                                #inicio el proceso para guardar el archivo shp en la base de datos
                                rutina = Shp2pgsql.run_subproceso("4326", check_file_shp[0], "PUBLIC", check_file_shp[1])
                                if rutina[0] == True:
                                    return Response({
                                        'message': 'Datos exportados correctamente.',
                                        'info': str(rutina[1])
                                    }, status=status.HTTP_200_OK)

                                return Response({
                                    'title': 'Exception',
                                    'code': '400',
                                    'message': 'no se pudo guardar el archivo en bd',
                                    'error': str(rutina[1])
                                    }, status=status.HTTP_400_BAD_REQUEST)

                            else:
                                return Response({
                                    'title': 'Error',
                                    'message': 'No se encontro archivo shp en la carpeta descomprimida',
                                    'info': str(check_file_shp)
                                }, status=status.HTTP_406_NOT_ACCEPTABLE)
                        else:
                            return Response({
                                'title': 'Error',
                                'message': 'No se pudo descomprimir el archivo',
                                'info': unzip
                            }, status=status.HTTP_406_NOT_ACCEPTABLE)
                    else:
                        return Response({
                            'title': 'Error',
                            'message': 'No se encontro archivos con extension shp dentro del zip',
                            'info': existe_shp
                        }, status=status.HTTP_406_NOT_ACCEPTABLE)
                else:
                    return Response({
                        'title': 'Not Acceptable',
                        'message': 'solo se aceptan archivos con extension .zip'
                    }, status=status.HTTP_406_NOT_ACCEPTABLE)
            else:
                return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated, IsAdminUser])
def receive_polyline(request):
    try:
        if request.method == 'POST':
            print('post', request.data)
            serializer = GeoJsonSerializer(data=request.data)

            if serializer.is_valid():
                feacture_json = serializer.data.get('geojson')
                table_name = serializer.data.get('name').replace(" ", "_") #prevenir nombres con espacio para generar la tabla
                #print('feacture_json', len(feacture_json))
                print('table_name', table_name)
                if len(feacture_json) > 0:
                    message = None
                    with connection.cursor() as cursor:
                        message = import_feature(cursor, feacture_json, table_name)
                    connection.commit()

                    return Response({'message': message}, status=status.HTTP_200_OK)
                else:
                    return Response({
                        'title': 'BAD REQUEST',
                        'code': str(status.HTTP_400_BAD_REQUEST),
                        'message': 'El GeoJson se encuentra vacio'
                    }, status=status.HTTP_400_BAD_REQUEST)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


def import_feature(cursor, feature_data, table_name):
    try:
        script_create_table = 'CREATE TABLE IF NOT EXISTS ' + str(table_name) + ' (gid SERIAL, geom geometry(geometry, 4326), properties TEXT, PRIMARY KEY(gid))'
        if feature_data.get('type') == 'FeatureCollection':
            print('dentro del if FeatureCollection')
            for feature in feature_data['features']:
                import_feature(cursor, feature, table_name)
        elif feature_data.get('type') == 'Feature':
            print('dentro del if Feature')
            geo_json = json.dumps(feature_data['geometry'])
            str_dict = json.dumps(dict((k, v) for k, v in feature_data['properties'].items()))
            cursor.execute(script_create_table)
            cursor.execute('INSERT INTO ' + str(table_name) + '(geom, properties) VALUES (ST_SetSRID(ST_GeomFromGeoJSON(\'' + geo_json +'\'), 4326), \'' + str_dict +'\')')
            print('capa creada....')
        return str("Capa creada exitosamente")

    except Exception as e:
        return str(e)