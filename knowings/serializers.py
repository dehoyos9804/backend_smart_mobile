from rest_framework import serializers
from .models import Knowing, LogRequest, LogFile
from django.utils import timezone


class CreateKnowingSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        knowing = Knowing.objects.create(**validated_data)
        return knowing

    class Meta:
        model = Knowing
        fields = ('id', 'address', 'city', 'cx', 'cy', 'formatted_address', 'created_at', 'deleted_at', 'query_type', 'state')
        extra_kwargs = {
            'address': {
                'required': True,
                'error_messages': {
                    'required': 'address es requerido'
                }
            },
            'city': {
                'required': False
            },
            'cx': {
                'required': False
            },
            'cy': {
                'required': False
            },
            'formatted_address': {
                'required': False
            },
            'created_at': {
                'required': False,
                'default': timezone.now()
            },
            'deleted_at': {
                'required': False,
                'default': timezone.now()
            },
            'state': {
                'default': '1',
            },
        }


class CreateLogRequestSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        log_request = LogRequest.objects.create(**validated_data)
        return log_request

    class Meta:
        model = LogRequest
        fields = (
            'id',
            'address',
            'city',
            'cx',
            'cy',
            'formatted_address',
            'query_type',
            'zoning_base',
            'zoning_value',
            'created_at',
            'user'
        )

        extra_kwargs = {
            'address': {
                'required': True,
            },
            'city': {
                'required': False
            },
            'cx': {
                'required': False
            },
            'cy': {
                'required': False
            },
            'formatted_address': {
                'required': False
            },
            'query_type': {
                'required': False,
            },
            'zoning_base': {
                'required': False,
            },
            'zoning_value': {
                'required': False,
            },
            'created_at': {
                'required': False,
                'default': timezone.now()
            },
            'user': {
                'required': False,
            },
        }


class GeocoderSerializer(serializers.Serializer):
    address = serializers.CharField(max_length=200)
    city = serializers.CharField(max_length=200)


class PluckingSerializer(serializers.Serializer):
    cx = serializers.CharField(max_length=200)
    cy = serializers.CharField(max_length=200)


class TableColumnSerializer(serializers.Serializer):
    table_name = serializers.CharField(max_length=200)


class LogFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogFile
        fields = ('id', 'content_type', 'size', 'extension', 'file', 'created_at', 'user', 'name')
        extra_kwargs = {
            'content_type': {
                'required': False,
            },
            'size': {
                'required': False,
            },
            'extension': {
                'required': False,
            },
            'file': {
                'required': True,
            },
            'created_at': {
                'required': False,
                'default': timezone.now()
            },
            'user': {
                'required': False,
            },
            'name': {
                'required': False,
            },
        }


class GeoJsonSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    geojson = serializers.DictField()