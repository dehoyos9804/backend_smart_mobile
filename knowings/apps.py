from django.apps import AppConfig


class KnowingsConfig(AppConfig):
    name = 'knowings'
