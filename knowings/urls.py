from django.shortcuts import render

# Create your views here.
from django.urls import path

from . import views

urlpatterns = [
    path('geocodificar_base/', views.geocodificar_base),
    path('plucking/', views.plucking),
    path('table_list/', views.table_list),
    path('table_list/columns/<str:table_name>', views.table_list_column),
    path('upload/file/shp/', views.uploader_file_shp),
    path('receive/polyline/', views.receive_polyline)
]