from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Knowing(models.Model):
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    cx = models.CharField(max_length=200)
    cy = models.CharField(max_length=200)
    formatted_address = models.CharField(max_length=200)
    created_at = models.DateTimeField(null=True)
    deleted_at = models.DateTimeField(null=True)
    query_type = models.CharField(max_length=2)
    state = models.CharField(max_length=1)

class LogRequest(models.Model):
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    cx = models.CharField(max_length=200)
    cy = models.CharField(max_length=200)
    formatted_address = models.CharField(max_length=200)
    query_type = models.CharField(max_length=2)
    zoning_base = models.CharField(max_length=200, blank=True, null=True)
    zoning_value = models.CharField(max_length=200, blank=True, null=True)
    user = models.ForeignKey(User,  on_delete=models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(null=True)

class LogFile(models.Model):
    content_type = models.CharField(max_length=200, null=True)
    size = models.CharField(max_length=200, null=True)
    extension = models.CharField(max_length=200, null=True)
    file = models.FileField(blank=True, null=True, unique=True)
    name = models.CharField(max_length=200, null=True, unique=True)
    created_at = models.DateTimeField(null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)


