from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth.models import User
from .serializers import UserSerizalizer
from rest_framework.pagination import PageNumberPagination

# Create your views here.
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdminUser])
def list(request):
    try:
        if request.method == 'GET':
            '''
            método que devuelve todos los usuarios
            '''
            page_size = None
            is_active = None
            is_staff = None

            if 'page_size' in request.data:
                page_size = request.data['page_size']

            if 'is_active' in request.data:
                is_active = bool(request.data['is_active'])

            if 'is_staff' in request.data:
                is_staff = bool(request.data['is_staff'])


            users = User.objects.get_queryset().order_by('id')

            if is_active is not None:
                users = users.filter(is_active=is_active)

            if is_staff is not None:
                users = users.filter(is_staff=is_staff)

            paginator = PageNumberPagination()

            if page_size is not None:
                paginator.page_size = page_size

            result_page = paginator.paginate_queryset(users, request)
            serializer = UserSerizalizer(result_page, many=True)

            return paginator.get_paginated_response(serializer.data)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo GET'
            }, 405)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, 400)

@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdminUser])
def user(request, pk):
    try:
        if request.method == 'GET':
            '''
            método un unico usuario con
            '''

            try:
                user = User.objects.get(pk=pk)

            except User.objects.DoesNotExist:
                return Response({
                    'error': 'Model',
                    'message': 'No éxiste el recurso'
                })

            serializer = UserSerizalizer(user)

            return Response({'data': serializer.data}, 200)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo GET'
            }, 405)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, 400)

@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def update(request, pk):
    try:
        if request.method == 'PATCH':
            '''
            método que permite actualizar los datos del usuario
            '''

            try:
                user = User.objects.get(pk=pk)

            except User.objects.DoesNotExist:
                return Response({
                    'error': 'Model',
                    'message': 'No éxiste el recurso'
                })

            if 'first_name' in request.data:
                user.first_name = request.data['first_name']

            if 'last_name' in request.data:
                user.last_name = request.data['last_name']

            if 'email' in request.data:
                user.email = request.data['email']

            if 'is_staff' in request.data:
                user.is_staff = bool(request.data['is_staff'])

            if 'is_active' in request.data:
                user.is_active = bool(request.data['is_active'])

            user.save()
            serializer = UserSerizalizer(user)
            data = {
                'title': 'Success',
                'message': 'Usuario actualizado',
                'data': serializer.data
            }

            return Response({'data': data}, 200)

        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo PATCH'
            }, 405)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, 400)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated, IsAdminUser])
def delete(request, pk):
    try:
        if request.method == 'DELETE':
            '''
            método que eliminar un usuario
            '''

            try:
                user = User.objects.get(pk=pk)

            except User.objects.DoesNotExist:
                return Response({
                    'error': 'Model',
                    'message': 'No éxiste el recurso'
                })

            user.is_active = False
            user.save()
            data = {
                'title': 'Success',
                'message': 'Usuario Eliminado',
            }

            return Response({'data': data}, 200)

        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo DELETE'
            }, 405)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, 400)