from django.urls import path
from . import views

urlpatterns = [
    path('list/', views.list),
    path('get/<int:pk>', views.user),
    path('update/<int:pk>', views.update),
    path('delete/<int:pk>', views.delete),
]