import os
from rest_framework import status
from utils.utilitarios import Utilitarios
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
import requests
from .serializers import CreateUserSerializer, UpdatePasswordSerializer
from django.contrib.auth.models import User
from backend_smartmobile.settings import DATABASE_ENV, ALLOWED_HOSTS, IP_HOST_CONFIG
from oauth2_provider.models import AccessToken

config = Utilitarios.read_file_json('./config.json')

if DATABASE_ENV.__eq__("LOCAL"):
    CLIENT_ID = config['LOCAL']['CLIENT_ID']
    CLIENT_SECRET = config['LOCAL']['CLIENT_SECRET']
    APP_USERNAME = config['LOCAL']['APP_USERNAME']
    APP_PASSWORD = config['LOCAL']['APP_PASSWORD']

elif DATABASE_ENV.__eq__("CLOUD"):
    CLIENT_ID = config['CLOUD']['CLIENT_ID']
    CLIENT_SECRET = config['CLOUD']['CLIENT_SECRET']
    APP_USERNAME = config['CLOUD']['APP_USERNAME']
    APP_PASSWORD = config['CLOUD']['APP_PASSWORD']


# create new user
@api_view(['POST'])
@permission_classes([IsAdminUser])
def register(request):
    try:
        if request.method == 'POST':
            # colocar los datos que vienen del request en el serializador
            serializar = CreateUserSerializer(data=request.data)

            if serializar.is_valid():
                #si los datos logran pasar la validación procedemos a guardarlo
                serializar.save()
                return Response({
                    'title': 'success',
                    'message': 'usuario registrado exitosamente'
                }, status=status.HTTP_200_OK)

            return Response(serializar.errors)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    try:
        if request.method == 'POST':
            url = ''
            if DATABASE_ENV.__eq__("LOCAL"):
                url = 'http://127.0.0.1:8000/o/token/'
            elif DATABASE_ENV.__eq__("CLOUD"):
                #ip = ALLOWED_HOSTS[0]
                ip = IP_HOST_CONFIG
                url = 'http://' + ip + '/o/token/'

            user = User.objects.filter(username=request.data['username']).first()

            r = requests.post(url,
                                  data={
                                      'grant_type': 'password',
                                      'expires_in': 2000,
                                      'username': request.data['username'],
                                      'password': request.data['password'],
                                      'client_id': CLIENT_ID,
                                      'client_secret': CLIENT_SECRET,
                                  }
                              )

            data = r.json()
            data['id'] = user.id
            data['username'] = user.username
            data['first_name'] = user.first_name
            data['last_name'] = user.last_name
            data['email'] = user.email
            data['is_staff'] = user.is_staff
            data['is_active'] = user.is_active

            return Response(data)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([AllowAny])
def auto_login(request):
    try:
        if request.method == 'POST':
            url = ''
            if DATABASE_ENV.__eq__("LOCAL"):
                url = 'http://127.0.0.1:8000/o/token/'
            elif DATABASE_ENV.__eq__("CLOUD"):
                ip = IP_HOST_CONFIG
                url = 'http://' + ip + '/o/token/'

            user = User.objects.filter(username=APP_USERNAME).first()

            r = requests.post(url,
                              data={
                                    'grant_type': 'password',
                                    'username': APP_USERNAME,
                                    'password': APP_PASSWORD,
                                    'client_id': CLIENT_ID,
                                    'client_secret': CLIENT_SECRET
                              })
            if r.status_code == status.HTTP_200_OK:
                data = r.json()
                data['id'] = user.id
                data['username'] = user.username
                data['first_name'] = user.first_name
                data['last_name'] = user.last_name
                data['email'] = user.email
                data['is_staff'] = user.is_staff
                data['is_active'] = user.is_active

                acces = AccessToken.objects.filter(token=data['access_token']).first()

                data['date_expire'] = acces.expires

                return Response(data)
            else:
                return Response({
                    'title': 'BAD REQUEST',
                    'code': status.HTTP_400_BAD_REQUEST,
                    'message': 'Ocurrio un error al realizar el auto login'
                }, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    try:
        if request.method == 'POST':

            url = ''
            if DATABASE_ENV.__eq__("LOCAL"):
                url = 'http://127.0.0.1:8000/o/token/'
            elif DATABASE_ENV.__eq__("CLOUD"):
                #ip = ALLOWED_HOSTS[0]
                ip = IP_HOST_CONFIG
                url = 'http://' + ip + '/o/token/'

            r = requests.post(url,
                              data={
                                  'grant_type': 'refresh_token',
                                  'refresh_token': request.data['refresh_token'],
                                  'client_id': CLIENT_ID,
                                  'client_secret': CLIENT_SECRET,
                              },
                              )
            return Response(r.json())
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '400',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@permission_classes([AllowAny])
def revoke_token(request):
    try:
        if request.method == 'POST':
            url = ''
            if DATABASE_ENV.__eq__("LOCAL"):
                url = 'http://127.0.0.1:8000/o/revoke_token/'
            elif DATABASE_ENV.__eq__("CLOUD"):
                #ip = ALLOWED_HOSTS[0]
                ip = IP_HOST_CONFIG
                url = 'http://' + ip + '/o/revoke_token/'

            r = requests.post(url,
                              data={
                                  'token': request.data['token'],
                                  'client_id': CLIENT_ID,
                                  'client_secret': CLIENT_SECRET,
                              },
                              )
            # If it goes well return sucess message (would be empty otherwise)
            if r.status_code == requests.codes.ok:
                return Response({
                    'title': 'ok',
                    'code': r.status_code,
                    'message': 'token revoked'
                }, r.status_code)
            # Return the error if it goes badly
            return Response(r.json(), r.status_code)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def restore_password(request):
    try:
        if request.method == 'PATCH':
            serializers = UpdatePasswordSerializer(data=request.data)
            if serializers.is_valid():
                user = User.objects.get(username=request.data.get('username'))

                if user.check_password(serializers.data['password']):
                    return Response({'message': 'La contraseña nueva es igual a la anterior'}, status= status.HTTP_403_FORBIDDEN)

                user.set_password(serializers.data.get('password'))
                user.save()
                return Response({'message': 'Contraseña actualizada.'}, status=status.HTTP_200_OK)
            return Response(serializers.errors)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo PATCH'
            }, status.HTTP_405_METHOD_NOT_ALLOWED)
    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)
