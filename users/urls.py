from django.urls import path

from . import views

urlpatterns = [
    path('register/', views.register),
    path('login/', views.login),
    path('auto_login/', views.auto_login),
    path('token/refresh/', views.refresh_token),
    path('token/revoke/', views.revoke_token),
    path('restore/password/', views.restore_password)
]