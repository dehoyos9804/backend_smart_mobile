from rest_framework import serializers
from django.contrib.auth.models import User

class CreateUserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'first_name', 'last_name', 'email', 'is_staff', 'is_active')
        extra_kwargs = {
            'username': {
                'required': True,
                'error_messages': {
                    'required': 'username es requerido'
                }
            },
            'password': {
                'required': True,
                'write_only': True,
                'error_messages': {
                    'required': 'password es requerido'
                }
            },
            'is_staff': {
                'default': False
            },
            'is_active': {
                'default': True
            }
        }

class UpdatePasswordSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=200)
    password = serializers.CharField(max_length=200)

    def validate(self, data):
        if not data.get('password'):
            raise serializers.ValidationError({'password': 'Falta agregar la nueva contraseña'})

        if not data.get('username'):
            raise serializers.ValidationError({'username': 'Falta agregar la username'})

        return data