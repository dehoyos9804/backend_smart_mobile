from django.urls import path

from . import views

urlpatterns = [
    path('request/user/', views.report_date_request)
]