from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from knowings.models import Knowing, LogRequest
from .serializers import ReportDateSerializer, ReportSerializer
from rest_framework.pagination import PageNumberPagination

@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdminUser])
def report_date_request(request):
    '''
        método que devuelve el total de peticiones que hizo un usuario por un rango de fecha dada
    '''
    try:
        if request.method == 'GET':
            serializer = ReportDateSerializer(data=request.data)

            if serializer.is_valid():
                page_size = None

                if 'page_size' in request.data:
                    page_size = request.data['page_size']

                paginator = PageNumberPagination()

                if page_size is not None:
                    paginator.page_size = page_size

                query = '''
                        SELECT 
                        1 as id,
                        COUNT(kn.user_id) AS total_peticiones,
                        kn.user_id,
                        au.first_name,
                        au.last_name,
                        au.email,
                        au.username
                        FROM knowings_logrequest kn
                        INNER JOIN auth_user au ON kn.user_id = au.id
                        WHERE kn.created_at::DATE>=%s
                        AND kn.created_at::DATE<=%s
                        GROUP BY kn.user_id, au.first_name, au.last_name, au.email, au.username
                        '''

                log_data = LogRequest.objects.raw(query, [serializer.data.get('fecha_inicial'), serializer.data.get('fecha_final')])

                result_page = paginator.paginate_queryset(log_data, request)
                seri = ReportSerializer(result_page, many=True)

                return paginator.get_paginated_response(seri.data)
            else:
                return Response(serializer.errors, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({
                'title': 'Method Not Allowed',
                'code': '405',
                'message': 'método no permitido, solo se acepta de tipo POST'
            }, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    except Exception as e:
        return Response({
            'title': 'Exception',
            'code': '400',
            'message': str(e)
        }, status=status.HTTP_400_BAD_REQUEST)
