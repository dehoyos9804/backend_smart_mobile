from rest_framework import serializers

class ReportDateSerializer(serializers.Serializer):
    fecha_inicial = serializers.DateField()
    fecha_final = serializers.DateField()

class ReportSerializer(serializers.Serializer):
    total_peticiones = serializers.IntegerField()
    user_id = serializers.IntegerField()
    first_name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)
    email = serializers.CharField(max_length=200)
    username = serializers.CharField(max_length=200)