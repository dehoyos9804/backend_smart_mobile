### SmartMobile
Proyecto smart monility ...

## Requisitos

- [**Python**](https://www.python.org/downloads/) 3.7.x
- [**virtualenv**](https://virtualenv.pypa.io/en/stable/) (Recomendado)

## Instalación de este repositorio

Clonar este repositorio y alojarlo en una carpeta conveniente.

```shell
git clone git@git.sitimapa.co:aldair.dehoyos/backend_smart_mobile.git
```

## Activar virtualenv entorno Windows

```shell
\> python -m venv env
\> env/scripts/activate
```

## Instalar las dependencias

Una vez dentro del entorno, instalar las dependencias:

```sh
(env) pip install -r requirements.txt
```

## Servicios

### Auth

para la autenticacion de los usuarios

### Registro de usuarios

###### POST

```apl
/auth/register/
```

###### BODY

```json
{
  "username": "dehoyos98045",
  "password": "123456789",
  "first_name": "Aldair luis",
  "last_name": "De Hoyos teran",
  "email": "dehoyos9804@gmail.com",
  "is_staff": "true"
}
```

NOTA: **is_staff** tiene dos estado "*`true`*, representa al usuario admin", "`false`, usuario regular"



### Login

###### POST

```apl
/auth/login/
```

###### BODY

```json
{
  "username": "dehoyos9804",
  "password": "123456789"
}
```

###### Response

```json
{
    "access_token": "ppZOhutkPUVXXFngPIQ9LX0lK8DrPn",
    "expires_in": 36000,
    "token_type": "Bearer",
    "scope": "read write",
    "refresh_token": "8D4FHiycBTCIfyQdgoDoVaDpIfVp5v"
}
```

### Revocar

###### POST

```apl
/auth/token/revoke/
```

###### BODY

```json
{
    "token":"R8GbzeEItokTfXjWKSt9xgqgh7lJWT"
}
```

###### Response

```json
{
    "title": "ok",
    "code": 200,
    "message": "token revoked"
}
```

### GEOCODIFICAR

###### POST

```apl
/api/knowing/geocodificar_base/
```

###### BODY

```json
{
    "address":"Bilingual School, Calle 1A Bis",
    "city": "Bogota",
    "zoning_base": "comunas_localidades_colombia_geomarketing",
    "zoning_value": "nombre"
}
```

Nota: **zoning_base** será la base para zonificar y **zoning_value** será el valor zonificado

###### Response

```json
{
    "data": {
        "address": "School, Calle 1A Bis, Localidad Los Mártires",
        "city": "Bogota",
        "cx": "4.597053",
        "cy": "-74.101924",
        "formatted_address": "Cl. 1b #2748, Bogotá, Colombia",
        "data_zonificado": {
            "nombre": "LOS MARTIRES",
            "codigo": "14"
        }
    }
}
```

### PUNTEO MANUAL

###### POST

```apl
/api/knowing/plucking/
```

###### BODY

```json
{
    "cx":"4.597053",
    "cy": "-74.101924",
    "zoning_base": "comunas_localidades_colombia_geomarketing",
    "zoning_value": "nombre,codigo"
}
```

Nota: **zoning_base** será la base para zonificar y **zoning_value** será el valor zonificado

###### Response

```json
{
    "data": {
        "address": "School, Calle 1A Bis, Localidad Los Mártires",
        "city": "Bogota",
        "cx": "4.597053",
        "cy": "-74.101924",
        "formatted_address": "Cl. 1b #2748, Bogotá, Colombia",
        "data_zonificado": {
            "nombre": "LOS MARTIRES",
            "codigo": "14"
        }
    }
}
```

### OBTENER TODOS LOS USUARIOS

###### GET

```apl
/api/users/list/
```

###### Response

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "username": "SmartMobility",
            "first_name": "",
            "last_name": "",
            "email": "mail@mail.com",
            "is_staff": true,
            "is_active": true
        },
        {
            "id": 2,
            "username": "dehoyos98045",
            "first_name": "Aldair luis",
            "last_name": "De Hoyos teran",
            "email": "dehoyos@mail.com",
            "is_staff": true,
            "is_active": true
        }
    ]
}
```

###### BODY (Campos opcionales)

```json
{
    "is_staff":true,
    "is_active": true,
    "page_size": 20,
}
```

1. **is_staff:** esta variable te permite filtrar todos los usuarios que sean de tipo administrador (`true`) o regular(`false`)
2. **is_active:** permite filtrar por usuarios activos(`true`) e inactivos(`false`)
3. **page_size**:  permite establecer el tamaño de la paginación por pagina

### OBTENER UN USUARIO ESPECIFICO

###### GET

```apl
api/users/get/<int:pk>
```

###### Response

```json
{
    "data": {
        "id": 2,
        "username": "dehoyos9804",
        "first_name": "Aldair luis",
        "last_name": "De Hoyos teran",
        "email": "dehoyos9804@gmail.com",
        "is_staff": true,
        "is_active": true
    }
}
```

### ACTUALIZAR UN USUARIO ESPECIFICO

###### PATCH

```apl
/api/users/update/<int:pk>
```

###### BODY (Campos opcionales)

```json
{
    "last_name": "LAST NAME PRUEBA"
}
```

> puedes mandar los parametros que son necesarios para actualizar. ejemplo: last_name, first_name, email..

###### Response

```json
{
    "data": {
        "title": "Success",
        "message": "Usuario actualizado",
        "data": {
            "id": 2,
            "username": "dehoyos9804",
            "first_name": "Aldair luis",
            "last_name": "Aldair de luis",
            "email": "dehoyos9804@gmail.com",
            "is_staff": true,
            "is_active": true
        }
    }
}
```

### ELIMINAR UN USUARIO

###### PATCH

```apl
/api/users/delete/3
```

###### Response

```json
{
    "data": {
        "title": "Success",
        "message": "Usuario Eliminado"
    }
}
```

### ACTUALIZAR CONTRASEÑA DE UN USUARIO

###### PATCH

```apl
/auth/restore/password/
```

###### BODY

```json
{
    "username": "dehoyos9804",
    "password": "@123456789"
}
```

###### Response

```json
{
    "message": "Contraseña actualizada."
}
```

### OBTENER TODAS LAS TABLAS PARA ZONIFICAR

###### GET

```apl
/api/knowing/table_list/
```

###### Response

```json
{
    "tables": [
        "comunas_localidades_colombia_geomarketing"
    ]
}
```

### OBTENER TODAS LAS COLUMNAS DE TABLAS

###### GET

```apl
/api/knowing/table_list/columns/<str:table_name>
```

###### Response

```json
{
    "table": "comunas_localidades_colombia_geomarketing",
    "columns": [
        "gid",
        "cartodb_id",
        "nombre",
        "codigo",
        "nom_cpob",
        "cod_cpob",
        "nom_mun",
        "cod_mun",
        "nom_dep",
        "cod_dpto",
        "codgeo",
        "geom"
    ]
}
```

### SUBIR NUEVO ARCHIVO GEOCODER

puedes subir archivos shp, es importante aclaras que se reciben un archivo zip, donde esten el archivo shp, dbf, shx..

**URL:** `/api/knowing/upload/file/shp/`

**Method:** `POST`

**Auth required** : YES

**BODY**

```json
{
	"file": "<file>"
}
```

###### Success Responses

**Condition** : Datos exportados correctamente.

**Code** : `200 OK`

**Content** :

```json
{
    "message": "Datos exportados correctamente.",
    "info": ""
}
```

### GUARDAR UN GEOJSON 

permite procesar un geojson y guardarlo en una tabla para futuras consultas 

**URL:** `/api/knowing/receive/polyline/`

**Method:** `POST`

**Auth required** : YES, SOLO USUARIOS ADMINISTRADORES

**BODY**

```json
{
    "name": "capa_02",
    "geojson": {"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[-74.07227923283708,4.644803300098172],[-74.06923224339616,4.644033360400848],[-74.06893183598649,4.646984791341889],[-74.07300879368913,4.647412533931443]]]},"properties":{"nombre":"campin","valor":10}},{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[-74.0739958466066,4.636291142306344],[-74.05768801579606,4.647968598909766],[-74.06197955021989,4.630174302264351]]]},"properties":{"nombre":"marly","valor":20}},{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[-74.08794333348405,4.6357778430786345],[-74.07459666142594,4.632569714459064],[-74.08768584141862,4.628463288587854]]]},"properties":{"nombre":"un","valor":30}}]}
}
```

###### Success Responses

**Condition** : Capa creada exitosamente.

**Code** : `200 OK`

**Content** :

```json
{
    "message": "Capa creada exitosamente"
}
```

### REPORTE TOTAL PETICIONES POR USUARIOS

permite que los usuarios administradores lleven un control por usuarios, mediante filtros de fechas

**URL:** `/api/reports/request/user/`

**Method:** `GET`

**Auth required** : YES, SOLO USUARIOS ADMINISTRADORES

**BODY**

```json
{
    "fecha_inicial":"2021-01-01",
    "fecha_final":"2021-04-07"
}
```

###### Success Responses

**Code** : `200 OK`

**Content** :

```json
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
            "total_peticiones": 3,
            "user_id": 2,
            "first_name": "Aldair luis",
            "last_name": "Aldair de luis",
            "email": "dehoyos9804@gmail.com",
            "username": "dehoyos9804"
        },
        {
            "total_peticiones": 1,
            "user_id": 3,
            "first_name": "Aldair luis",
            "last_name": "De Hoyos teran",
            "email": "dehoyos9804@gmail.com",
            "username": "dehoyos98045"
        }
    ]
}
```

